# currency_bump_plot

This repository contains two python scripts.

**download_top_300.py**
This scripts downloads the currently top300 ranked cryptocurrencies from coinmarketcap.com using the REST API. An API-Key is required for this and must be stored in the "api-key" file. The ranking is stored in a textfile, seperated by newlines, using the file-extension ".cmdata". The filename is the current date and time.

**bump_chart_top_300.py**
This scripts takes all .cmdata files that can be found and creates a bump-chart for the newest top 300. The bump chart is storen in a .png-file.
