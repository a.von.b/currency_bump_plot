__author__ = "a.vonbergner@gmx.de"
__date__ = "03.05.2021"
__license__ = "MIT"

from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
from datetime import datetime
import json

datetime_string_format = "%Y_%m_%d-%H_%M_%S"

if __name__ == "__main__":
    # read api key
    api_key = open("api_key", 'r').read()
    count = 300

    # prepare API call
    url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
    parameters = {
        'start': '1',
        'limit': str(count),
        'convert': 'USD'
    }
    headers = {
        'Accepts': 'application/json',
        'X-CMC_PRO_API_KEY': api_key,
    }
    session = Session()
    session.headers.update(headers)

    # open file for writing
    output_file_name = datetime.now().strftime(datetime_string_format + ".cmdata")
    output_file = open(output_file_name, 'w')

    # API call
    try:
        response = session.get(url, params=parameters)
        data = json.loads(response.text)
        # write data to file
        for d in data["data"]:
            output_file.write(d["name"] + "\n")
        # save file
        output_file.close()

    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)
