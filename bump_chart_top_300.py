__author__ = "a.vonbergner@gmx.de"
__date__ = "03.05.2021"
__license__ = "MIT"

import os
from download_top_300 import datetime_string_format
from matplotlib import pyplot as plt
import numpy

from datetime import datetime
from datetime import timedelta

plot_number = 300


class currency_rating:
    """Class for storing a rank of a currency on a specific date
    """

    def __init__(self, date, rank):
        self.date = date
        self.rank = rank


class history_currency_rating:
    """Class for storing the rank of one currency over multiple days
    """

    def __init__(self, name, date, rank):
        self.name = name
        self.history_rankings = [currency_rating(date, rank)]

    def add_ranking(self, date, rank):
        self.history_rankings.append(currency_rating(date, rank))


class currency_bump_chart_data:
    """Class for storing multiple currencies and track their ranking over multiple days.
    """

    def __init__(self, date, currency_list):
        self.currencies = []
        for rank, currency in enumerate(currency_list):
            self.currencies.append(
                history_currency_rating(currency, date, rank))

    def add_measurement_point(self, date, currency_list):
        """Add a ranking of currencies to this data structure

        Args: 
            date (datetime): The date
            currency_list (list[string]): a newline seperated string list in ranked order (highest rank first)
        """
        for rank, c in enumerate(currency_list):
            # find index of currency in our data structure
            try:
                index = [
                    currency.name for currency in self.currencies].index(c)
                # print(index)
                self.currencies[index].add_ranking(date, rank)
            except(ValueError) as e:
                pass  # currency is not in our newest top300 list

    def __debug_print(self):
        """print a formatted version of the data structure to command line.
        """
        # debug print data
        for d in self.currencies:
            print(d.name)
            for h in d.history_rankings:
                print("  " + h.date.strftime("%Y-%m-%d"))
                print("  " + str(h.rank))
                print("  ---")
            print("-----")


if __name__ == "__main__":
    # find availiable data files
    file_list = []
    for file in os.listdir():
        if file.endswith(".cmdata"):
            file_list.append(file)

    # sort list (from old to new)
    file_list.sort()

    # read the newest top 300
    current_date = datetime.strptime(
        file_list[-1].split(".")[0], datetime_string_format).date()
    end_date = current_date
    with open(file_list[-1], 'r') as newest_file:
        top_300_list = newest_file.read().splitlines()

    # create data structure
    data = currency_bump_chart_data(current_date, top_300_list)

    # fill data structure with history data
    for cmfile_name in reversed(file_list):
        # extract date from filename
        file_date = datetime.strptime(cmfile_name.split(
            ".")[0], datetime_string_format).date()
        # we only accept the newest datapoint per day.
        if file_date <= current_date - timedelta(days=1):
            # read file
            with open(cmfile_name, 'r') as current_file:
                currency_list = current_file.read().splitlines()
            # parse into data structure
            data.add_measurement_point(file_date, currency_list)
            # remember that we have a datapoint for this date
            current_date = file_date
    start_date = current_date

    # plot
    fig = plt.figure(figsize=(10, 100))
    ax = plt.subplot(111)
    lines = []
    for currency in data.currencies[:plot_number]:
        line = plt.plot([history_ranking.date for history_ranking in currency.history_rankings],
                        [-history_ranking.rank for history_ranking in currency.history_rankings])
        lines.append(line)

    # beautifications for the plot
    plt.yticks([0, -49, -99, -149, -199, -249, -299],
               [1, 50, 100, 150, 200, 250, 300])
    plt.xticks(numpy.arange(start_date, end_date +
               timedelta(days=2), timedelta(days=1)))
    ax.xaxis.grid()
    # set labels for each currency
    for i, c in enumerate(top_300_list):
        plt.text(lines[i][-1].get_xdata()[0], lines[i]
                 [-1].get_ydata()[0], c, verticalalignment="center")
    # save image
    plt.savefig("bump_chart.png")
